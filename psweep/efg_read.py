import subprocess as sp
import ast
import numpy as np

# GATHER OUTPUT
def efg_read(block, params, dirs,):
    f_out = open(block + '/' + 'efg.csv', 'w')
    for i in range(len(params)):
        path = block + '/' + str(dirs[i]) + '/EFG.OUT'
        f = open(path, 'r')
        efg_data = f.readlines()
        f.close()

        EVs = {}
        for j in range(len(efg_data)):
            l = efg_data[j]
            if 'Species' in l:
                species = l[l.index('(')+1:l.index(')')]\
                          + l[l.index(':', l.index(','))+1:].strip()
                eigenvalues = np.array([float(k) \
                                        for k in efg_data[j+8].split()])
                EVs[species] = eigenvalues[np.argmax(abs(eigenvalues))]

        # Write top row
        if i == 0:
            # Create top row for output file
            top_row = block
            for key in EVs:
                top_row = top_row + ',' + key
            f_out.write(top_row + '\n')
            
        data_line = str(params[i])
        for key in EVs:
            data_line = data_line + ',' + str(EVs[key])
        f_out.write(data_line + '\n')
    f_out.close()

