import subprocess as sp

# Create inputs for parameter sweep
def psweep(input_block,
           params,
           output_dirs,
           elkin = './elk_template.in',
           slurm = './script_template.slurm'
          ):

    sp.call(['mkdir', '-p', input_block])

    for i in range(len(params)):
        f = open(elkin, 'r')
        lines = f.readlines()
        f.close()
        
        for e in lines:
            if input_block in e:
                ib_index = lines.index(e)
        lines[ib_index+1] = '  ' + params[i] + '\n'
    
        d = output_dirs[i]
        
        sp.call(['mkdir', '-p', input_block + '/' + d])
        
        f = open(input_block + '/' + d + '/' + 'elk.in', 'w')
        for line in lines:
            f.write(line)
        f.close()
        
        job_name = input_block + str(i)
        slurm_loc = input_block + '/' + d + '/script.slurm'
        work_dir = input_block + '/' + d
        sp.call(['cp', slurm, slurm_loc])
        sp.call(['sbatch', '-J', job_name, '-D', work_dir, slurm_loc])

